package android.example.com.waffle;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class WaffleDetails extends Fragment {
    TextView tv;

    public View onCreateView(LayoutInflater inflater, ViewGroup vg, Bundle savedInstanceState){

        //creating the view object & required data
        View rootView = inflater.inflate(R.layout.waffle_detail_fragment, vg,false);

        //getting the textView
        tv = rootView.findViewById(R.id.waffle_details);

        //getting stored data
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(getActivity());

        //registering listener to set textView
        pref.registerOnSharedPreferenceChangeListener(new SharedPreferences.OnSharedPreferenceChangeListener() {
            @Override
            public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {

                // can use the key returned by the listener
                // second var is a default to return if the key is not stored in the preferences
                String selected = sharedPreferences.getString(key, getString(R.string.Wtf)); // getting String
                System.out.println(selected);

                switch (selected) {
                    case "Blueberry Waffles":
                        tv.setText(getString(R.string.BUWaffles));
                        break;
                    case "Blackberry Waffles":
                        tv.setText(getString(R.string.BLWaffles));
                        break;
                    case "Strawberry Waffles":
                        tv.setText(getString(R.string.STWaffles));
                        break;
                    case "Banana Waffles":
                        tv.setText(getString(R.string.BAWaffles));
                        break;
                    case "Chocolate Waffles":
                        tv.setText(getString(R.string.CHWaffles));
                        break;
                    case "Whip cream Waffles":
                        tv.setText(getString(R.string.WCWaffles));
                        break;
                    case "Belgium Waffles":
                        tv.setText(getString(R.string.Belgium));
                        break;
                    case "Cream Cheese Waffles":
                        tv.setText(getString(R.string.CCWaffles));
                        break;
                    case "Waffles":
                        tv.setText(getString(R.string.Wtf));
                        break;
                }
            }
        });
        //Return the view
        return rootView;
    }
}