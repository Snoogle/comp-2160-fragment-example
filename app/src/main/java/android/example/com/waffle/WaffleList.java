package android.example.com.waffle;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

import static android.content.ContentValues.TAG;


public class WaffleList extends Fragment {

    public static String message = "default";

    public WaffleList(){
        //REQUIRED CONSTRUCTOR
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup vg, final Bundle savedInstanceState){

        //creating the view object
        View rootView = inflater.inflate(R.layout.waffle_list_fragment, vg,false);

        //creating the stored data
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
        final SharedPreferences.Editor editor = prefs.edit();

        //instantiating the list items
        final ArrayList<String> waffleTypes = new ArrayList<>();
        waffleTypes.add("Blueberry Waffles");
        waffleTypes.add("Blackberry Waffles");
        waffleTypes.add("Strawberry Waffles");
        waffleTypes.add("Banana Waffles");
        waffleTypes.add("Chocolate Waffles");
        waffleTypes.add("Whip cream Waffles");
        waffleTypes.add("Belgium Waffles");
        waffleTypes.add("Cream Cheese Waffles");

        //Grabbing ListView and setting the adapter
        ListView lv = rootView.findViewById(R.id.waffle_list);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, waffleTypes);
        lv.setAdapter(adapter);

        //Grabbing the item clicked and passing it on.
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                //Grab the item that was clicked and pass to the bundle
                message = waffleTypes.get(position);

                //storing data
                editor.putString("message_data", message);
                editor.apply();

                Log.d(TAG, "onCreateView: " + message);

            }
        });

        //Return the view.
        return rootView;
    }
}
